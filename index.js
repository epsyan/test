const express = require('express');
const path = require('path');
const app = express();
const PORT = process.env.PORT || 5000;

app
    .get('/', (req, res) => res.sendFile(`${path.dirname(__filename)}/public/index.html`))
    .listen(PORT);

app.use(express.static(__dirname + '/public'));